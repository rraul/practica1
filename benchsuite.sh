#!/usr/bin/env bash
size=10240
size2=10240000
op=sum
nthreads=8
case "$1" in
	st1)
		time ./base $size $op
		;;
	mt1)
		time ./base $size $op --multi-thread $nthreads
		;;
	st2)
		time ./base $size2 $op
		;;
	mt2)
		time ./base $size2 $op --multi-thread $nthreads
		;;
esac
