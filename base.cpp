#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include <string.h>
#include <thread>
#include <iostream>
#include <unistd.h>
#include <mutex>
#include <condition_variable>
#include <atomic>

enum Operacion{Sum, Xor, Nada};
enum Case{help, wrongNumberThread, unknownCommand, wrongLengthOperation};

bool debug = false, loggerF = false;
std::mutex mutexThreadLogger;
std::condition_variable varCond;
bool boolCond = false;
bool boolCond2 = false;
double resultadoLock;
int numThreadLock;

void notify(Case c);
void opera(Operacion operacion, double *arrayPunt, double *arrayPuntRes, int valoresPorThread, int numThread, std::mutex *mutexResFinal);
void logger(int numThreads, std::mutex *mutexLoggerMain, std::condition_variable *varCond2, bool *boolCond2, double *resultadoFinalLock);


int main(int argc, char *argv[]){

  srand(time(NULL));
  //Numero de threads Predeterminado = 1
  int numThread = 1, arrayLength= -1;
  Operacion operacion = Nada;

  //Leemos e identificamos los argumentos
  for (int i=1; i<argc; i++){
    if(strcmp("-logger", argv[i]) == 0){
      loggerF = true;
    }else if(strcmp("-debug", argv[i]) == 0){
      debug = true;
    }else if(strcmp("--help", argv[i]) == 0){
      notify(help);
      exit(0);
    }else if(*argv[i] >= '0' && *argv[i] <= '9'){
      arrayLength =  atoi(argv[i]);
    }else if(strcmp("sum", argv[i]) == 0){
      operacion = Sum;
    }else if(strcmp("xor", argv[i]) == 0){
      operacion = Xor;
    }else if(strcmp("--multi-thread", argv[i]) == 0){
      i++;
      if( *argv[i] >= '0' && *argv[i] <= '9'){
        numThread = atoi(argv[i]);
      }else{
        //Si despues de poner multi-thread no han puesto bien el numero
        notify(wrongNumberThread);
        exit(0);
      }}else{
        //Si hay algun comando que no se identifica
        notify(unknownCommand);
        exit(0);
      }
    }
    //Si no se ha seleccionado la operacion o la longitud del array
    if(operacion == Nada || arrayLength == -1){
      notify(wrongLengthOperation);
      exit(0);
    }

    if(debug){
      printf("Has activado el modo DEBUG\n\n" );
      printf("La longitud del Array sera de: %d numeros\n", arrayLength);
      if(operacion==Sum) printf("Has Seleccionado Suma \n");
      if(operacion==Xor) printf("Has Seleccionado XOR \n");
      printf("Has Selecionado: %d threads\n", numThread);
      if(logger) printf("Has activado el Feature Logger\n\n\n\n");
    }

    //Reservamos espacio para el array
    double *arrayPunt = (double *) malloc(arrayLength* sizeof(double));
    if(debug) printf("Reservando espacio en Memoria para el array %d elementos [Puntero: %p]\n",arrayLength, arrayPunt);
    for (int i=0; i<arrayLength; i++ ){
      arrayPunt[i]= i;
      if(debug) printf("Array[%d] = %f |", i, arrayPunt[i]);
    }
    if(debug) printf("\n");


    std::thread threads[numThread];
    if(debug) printf("%d Thread Creado(s)\n",numThread);

    //Calculamos la cantidad de numeros que tienen que procesar los threads
    //Si el calculo esta descomponsado, la carga se la lleva el primer thread
    int valoresPorThread = arrayLength/numThread, trabajoPrimerThread = valoresPorThread;
    //Si estan descompensados los trabajos
    if(valoresPorThread * numThread != arrayLength){
      trabajoPrimerThread = (arrayLength - (numThread*valoresPorThread)) + valoresPorThread ;
      //Si solo puede trabajar el primer thread
      if(trabajoPrimerThread < 0){
        trabajoPrimerThread=arrayLength;
      }
    }
    if(debug) printf("Trabajo Para el primer Thread  = |%d| doubles\n",trabajoPrimerThread);
    if(debug) printf("Trabajo Para el resto de Thread =|%d| doubles * %d (numThreads-1)\n\n",valoresPorThread, numThread-1);

    //Variables para que el logger pase el resultado al main
    std::mutex mutexLoggerMain;
    std::condition_variable varCond2;
    double resultadoFinalLock;

    //Creamos el hilo logger para que recoja los resultados
    std::thread threadLogger = std::thread(logger, numThread, &mutexLoggerMain, &varCond2, &boolCond2, &resultadoFinalLock);

    double puntResFinal;

    std::mutex mutexResFinal;

    //Creamos el primer thread y le especificamos el trabajo y aumentamos el puntero al array
    threads[0] = std::thread (opera, operacion, arrayPunt, &puntResFinal, trabajoPrimerThread, 0, &mutexResFinal);
    arrayPunt += trabajoPrimerThread;
    //Ejecutamos los threads restantes con sus cargas de valores equivalentes
    for (int i = 1; i<numThread; i++){
      threads[i] = std::thread (opera, operacion, arrayPunt, &puntResFinal, valoresPorThread, i, &mutexResFinal);
      arrayPunt = arrayPunt + valoresPorThread;
    }

    std::unique_lock<std::mutex> ulk2(mutexLoggerMain);
    varCond2.wait(ulk2, []{return boolCond2;});
    ulk2.unlock();
    printf("RESULTADO FINAL LOGGER %f\n", resultadoFinalLock);

    for (int i = 0; i<numThread; i++){
      threads[i].join();
    }
    threadLogger.join();

    //  double resultadoMain;
    //  arrayPuntRes = arrayPuntRes - numThread;
    //  for(double i = 1; i<=numThread; i++){
    //    //resultadoMain += i;
    //    resultadoMain += *arrayPuntRes;
    //    arrayPuntRes++;
    //  }
    printf("RESULTADO FINAL CALCULADO SECUENCIAL %f\n", puntResFinal);
  }

  // cambiar array resultados que lo vayan sumando el trabjador añadir mutex

  //Analisis meter if de sum o xor en el for o usar dos for para cada operacion como lo tengo ahora
  void opera(Operacion operacion, double *arrayPunt, double *puntResFinal, int valoresPorThread, int numThread, std::mutex *mutexResFinal){
    double res;

    if(operacion == Sum){ //Suma
      for (int i = 0; i<valoresPorThread; i++){
        res = res + *arrayPunt;
        arrayPunt = arrayPunt + 1;
      }}else{
        for (int i = 0; i<valoresPorThread; i++){
          res = (int)res ^ (int)*arrayPunt;
          arrayPunt = arrayPunt +1 ;
        }//----------Cuidado con el xor??????
      }

      //Enviamos resultado
      {
        if(debug)printf("Hilo %d Esperando\n", numThread);
        std::unique_lock<std::mutex> ulk(mutexThreadLogger); //Intentamos coger el mutex
        varCond.wait(ulk, []{return !boolCond;}); //Esperamos a que el logger nos de la señal
        if(debug) printf("Hilo %d Escribiendo Resultado\n", numThread);
        resultadoLock= res; numThreadLock= numThread; //Escribimos resultado
        boolCond = true; //Cambiamos la condicion y avisamos al thread que hemos escrito
        varCond.notify_all();
      }
      mutexThreadLogger.unlock(); //Desbloqueamos el mutex


      if(debug) printf("Thread %d Resultado = %f\n",numThread, res);
      if(debug) printf("Calculando y guardando el resultado final\n");


      std::unique_lock<std::mutex> ulk4(*mutexResFinal); //Intentamos coger el mutex
      if(operacion==Sum) *puntResFinal += res;
      if(operacion==Xor) *puntResFinal = (int)*puntResFinal ^ (int)res;
      ulk4.unlock();

      if(debug) printf("HILO %d Acabado^^\n",numThread);
    }


    void logger(int numThreads, std::mutex *mutexLoggerMain, std::condition_variable *varCond2, bool *boolCond2, double *resultadoFinalLock){
      if(debug) printf("Hilo Logger Creado\n");
      //Creamos las variables para recoger los resultados
      int numeroResultadosRecibidos = 0;
      double resultadoFinal;

      double *registro = (double *) malloc(numThreads * sizeof(double));

      //Recibimos resultados de los hilos
      while(numeroResultadosRecibidos<numThreads){
        std::unique_lock<std::mutex> ulk(mutexThreadLogger); // Entramos en el lock
        if(debug) printf("Logger esperando\n");

        varCond.wait(ulk, []{return boolCond;}); // Esperamos a que un hilo nos avise //-----------XQ NO USAMOS LAMBDA??? WTF
        resultadoFinal += resultadoLock; numeroResultadosRecibidos++; //Cargamos el resultado

        //REGISTRO
        *(registro + numThreadLock) = resultadoLock;
        if(debug) printf("escribiendo %p = %f\n",(registro + (numThreadLock * sizeof(double))), *(registro + numThreadLock));
        printf("LOGGER: Hilo[%d] Resultado = %f\n",numThreadLock,resultadoLock);

        boolCond = false; // Nos preparamos para recibir otro resultado
        varCond.notify_one(); //Notificamos a los hilos y desbloqueamos el mutex
      }
      mutexThreadLogger.unlock();
      //-------------Imprimir por registro de creacion no por llegada
      printf("Logger Acaba de rocoger todos los resultados resultado = %f\nEnviando Resultado... \n", resultadoFinal );

      for(int i=0; i<numThreads; i++){
        printf("REGISTRO LOGGER: Hilo[%d] Resultado = %f\n",i,*registro);
        registro += 1;
      }

      //Enviamos resultado final al main
      {
        std::lock_guard<std::mutex> lock(*mutexLoggerMain);
        *resultadoFinalLock = resultadoFinal;
        *boolCond2 = true;          // variable condicional no global ???
        varCond2->notify_one();
      }
    }


    void notify(Case c){
      if(c == help){
        printf("Has seleccionado la documentacion del programa:\n\n");
        printf("Base es un programa que permite al usuario operar una serie de arrays genreados al azar y que sean operados por diferentes threads a la vez. Se pueden sumar o Xor \n\n\n");
        printf("Comandos disponibles:\n");
        printf("    <X>: Numero entero que define el tamaño de array [OBLIGATORIO] \n");
        printf("    sum/xor: Expecifica la operacion [OBLIGATORIO] \n");
        printf("    --multi-thread <X>: Selecciona el numero de threads a usar [1 thread Predeterminado]\n");
        printf("    -logger: Activa el logger que obtiene los resultados de cada hilo\n");
        printf("    -debug: Activa la opcion para mostrar datos de ejecucion\n");
        printf("    --help: Muestra la documentacion del programa\n");
        printf("                                           Raul Ruiz\n");
      }else{
        switch (c){
          case(wrongNumberThread): printf("Error al introducir el numero de threads en: --multi-thread\n"); break;
          case(unknownCommand): printf("Error comando NO Reconocido. Vuelva a intentarlo \n"); break;
          case(wrongLengthOperation): printf("Error no has Seleccionado ninguna Operacion o longitud de Array\n"); break;
        }
        printf("Para ver la ayuda del programa use el comando --help (./base --help) \n");
      }
    }

    //La optimizacin del parelelismo se aplica en el base y viene reflejada en el tema2

    /*Main genera hilos trabajadores    OK
    Estos compuntan y sacan dos valores   OK
    Ademas genera el lloger que se queda en espera a que estos threds terminen   OK
    Cuando los que computan acaban avisan al logger quien es y el resultado. OK
    Una vez que hayan acabado todos, este muestre por terminal los resultados en order OK
    de llegada, despues denotificar, los suma y se lo pasa en main por condicion variable.  OK */
